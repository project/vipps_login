/**
 * @file
 * Simple JavaScript file.
 */
(function($, Drupal) {

  "use strict";

  $('#login-with-vipps-link').click(function (e) {
    e.preventDefault();

    var url = $(this).attr('href');

    $.ajax({ type: "GET",
      url: url,
      success: function(response){

        if(response.success) {
          window.location.href = response.url;
        } else {
          console.log(response.error);
        }
      }
    });
  });

})(jQuery, Drupal);
