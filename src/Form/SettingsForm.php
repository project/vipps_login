<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm
 */
class SettingsForm extends ConfigFormBase
{
  public const SETTINGS = 'vipps_login.settings';

  protected $config;

  public function __construct(ConfigFactoryInterface $config_factory)
  {
    parent::__construct($config_factory);

    $this->config = $this->config(static::SETTINGS);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'example_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['clientId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('clientId'),
      '#required' => true,
      '#description' => $this->t('You find this in the Vipps Portal.'),
    ];

    $form['clientSecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('clientSecret'),
      '#required' => true,
      '#description' => $this->t('You find this in the Vipps Portal.'),
    ];

    $form['testMode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test mode'),
      '#default_value' => $config->get('testMode'),
      '#description' => $this->t('Send requests to test server'),
    ];

    $form['showInLoginFrom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Vipps to login page'),
      '#default_value' => $config->get('showInLoginFrom'),
      '#description' => $this->t('Log in with Vipps on the login page'),
    ];

    $form['postLogin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Post login path'),
      '#default_value' => $config->get('postLogin') ?? '/user',
      '#required' => true,
      '#description' => $this->t('Path where the user should be redirected after a successful login. It must begin with /, # or ?.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('clientId', $form_state->getValue('clientId'))
      ->set('clientSecret', $form_state->getValue('clientSecret'))
      ->set('showInLoginFrom', $form_state->getValue('showInLoginFrom'))
      ->set('testMode', $form_state->getValue('testMode'))
      ->set('postLogin', $form_state->getValue('postLogin'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $postLoginFirstSymbol = substr($form_state->getValue('postLogin'), 0, 1);

    if (!in_array($postLoginFirstSymbol, ['/', '#', '?'])) {
      $form_state->setErrorByName('postLogin', $this->t('Post login path must begin with /, # or ?.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }
}
