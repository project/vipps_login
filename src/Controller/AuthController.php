<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\Messenger;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\vipps_login\Service\VippsLoginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\vipps_login\Service\Logger as VippsLoginLogger;

class AuthController extends ControllerBase
{
  protected $request;

  private $loginManager;

  private $logger;

  public function __construct(
    VippsLoginManager $loginManager,
    RequestStack $requestStack,
    Messenger $messenger,
    VippsLoginLogger $logger
  )
  {
    $this->loginManager = $loginManager;
    $this->request = $requestStack->getCurrentRequest();
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  public function getAuthRedirect(): JsonResponse {
    try {
      return new JsonResponse([
        'success' => true,
        'url' => $this->loginManager->getAuthRedirect(),
      ]);
    } catch (\Throwable $e) {
      return new JsonResponse([
        'success' => false,
        'error' => $e->getMessage(),
      ]);
    }
  }

  public function continueFromVipps() {
    try {
      $response = $this->loginManager->authenticateUser($this->request->get('code'));

      $this->messenger->deleteAll();

      return $response;

    } catch (\Throwable $e) {
      $this->messenger->addError($this->t('Something went wrong, please contact the administrator'));
      $this->logger->addError($e->getMessage());
      return $this->redirect('user.login');
    }
  }

  public static function create(ContainerInterface $container)
  {
    /* @var VippsLoginManager $loginManager */
    $loginManager = $container->get('vipps_login:manager');

    /* @var RequestStack $requestStack */
    $requestStack = $container->get('request_stack');

    /* @var $messenger Messenger */
    $messenger = $container->get('messenger');

    /* @var $logger VippsLoginLogger */
    $logger = $container->get('vipps_login:logger');

    return new static($loginManager, $requestStack, $messenger, $logger);
  }
}
