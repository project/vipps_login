<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\vipps_login\Form\SettingsForm;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigManager
{
  private $clientId;

  private $clientSecret;

  private $testMode;

  private $request;

  private $urlGenerator;

  private $postLoginUrl;

  public function __construct(
    ConfigFactoryInterface $configFactory,
    RequestStack $requestStack,
    UrlGeneratorInterface $urlGenerator
  )
  {
    $config = $configFactory->get(SettingsForm::SETTINGS);

    $this->clientId = $config->get('clientId');
    $this->clientSecret = $config->get('clientSecret');
    $this->postLoginUrl = $config->get('postLogin');
    $this->testMode = boolval($config->get('testMode'));
    $this->request = $requestStack->getCurrentRequest();
    $this->urlGenerator = $urlGenerator;
  }

  public function isTest(): bool {
    return $this->testMode;
  }

  public function getClientId(): ?string {
    return $this->clientId;
  }

  public function getClientSecret(): ?string {
    return $this->clientSecret;
  }

  public function getPostLoginUrl(): ?string {
    return $this->postLoginUrl;
  }

  public function getRedirectUrl(): string {
    return $this->request->getSchemeAndHttpHost() .
      $this->urlGenerator->generateFromRoute('vipps_login.continue_from_vipps');
  }

  public function generateUrl(string $action, array $args = null):string {
    $url = $this->getApiUrl() . $action;
    if(!is_null($args)) {
      $url .= '?' . http_build_query($args);
    }

    return $url;
  }

  public function getAuthorizationStringForAuthToken(): string {
    return "Basic " . base64_encode("{$this->clientId}:{$this->clientSecret}");
  }

  public function getAuthorizationStringForUserInfo(string $accessToken): string {
    return "Bearer {$accessToken}";
  }

  private function getApiUrl() :string {
    $domain = $this->isTest() ? 'apitest' : 'api';

    return sprintf("https://%s.vipps.no/access-management-1.0/access/", $domain);
  }

  private function credentialsGuard():void {
    if(empty($this->clientId) || empty($this->clientSecret)) {
      throw new \DomainException('Credentials are empty');
    }
  }

}
