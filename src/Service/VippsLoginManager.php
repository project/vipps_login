<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Service;

use Drupal\social_auth\User\UserAuthenticator;
use Drupal\vipps_login\ApiResponseData\AddressData;
use Drupal\vipps_login\ApiResponseData\UserData;
use Drupal\vipps_login\Service\Logger as VippsLoginLogger;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class VippsLoginManager
{
  private $configManager;

  private $stateKey;

  private $customerSession;

  private $httpClient;

  private $userAuthenticator;

  private $logger;

  const VERSION = '1.12';

  public function __construct(
    ConfigManager $configManager,
    StateKey $stateKey,
    CustomerSession $customerSession,
    ClientInterface $httpClient,
    UserAuthenticator $userAuthenticator,
    VippsLoginLogger $logger
  ) {
    $this->configManager = $configManager;
    $this->stateKey = $stateKey;
    $this->customerSession = $customerSession;
    $this->httpClient = $httpClient;
    $this->userAuthenticator = $userAuthenticator;
    $this->logger = $logger;
  }

  public function getAuthRedirect(string $scope="openid address email name phoneNumber"):string {

    $state = $this->stateKey->generate();
    $this->customerSession->setKeyState($state);

    $args = [
      'client_id' => $this->configManager->getClientId(),
      'response_type' => 'code',
      'scope' => $scope,
      'redirect_uri' => $this->configManager->getRedirectUrl(),
      'state' => $state,
    ];

    return $this->configManager->generateUrl('oauth2/auth', $args);
  }

  public function getAuthToken(string $code): string {
    $url = $this->configManager->generateUrl('oauth2/token');

    $args = [
      'grant_type' => 'authorization_code',
      'code' => $code,
      'redirect_uri' => $this->configManager->getRedirectUrl()
    ];

    $response = $this->httpClient->request('POST', $url, [
      'headers' => [
        'Authorization' => $this->configManager->getAuthorizationStringForAuthToken(),
        'Vipps-System-Name' => 'drupal',
        'Vipps-System-Version' => \Drupal::VERSION,
        'Vipps-System-Plugin-Name' => 'vipps-login',
        'Vipps-System-Plugin-Version' => $this::VERSION,
      ],
      'form_params' => $args,
    ]);

    return $this->getResponseBody($response)->access_token;
  }

  public function getUserInfo(string $accessToken): UserData {
    $url = $this->configManager->generateUrl('userinfo');

    $response = $this->httpClient->request('GET', $url, [
      'headers' => [
        'Authorization' => $this->configManager->getAuthorizationStringForUserInfo($accessToken),
        'Vipps-System-Name' => 'drupal',
        'Vipps-System-Version' => \Drupal::VERSION,
        'Vipps-System-Plugin-Name' => 'vipps-login',
        'Vipps-System-Plugin-Version' => $this::VERSION,
      ],
    ]);

    $responseBody = $this->getResponseBody($response);
    $addressBody = $responseBody->address[0];

    return new UserData(
      new AddressData(
        $addressBody->address_type,
        $addressBody->country,
        $addressBody->formatted,
        $addressBody->postal_code,
        $addressBody->region,
        $addressBody->street_address
      ),
      $responseBody->email_verified,
      $responseBody->email,
      $responseBody->given_name,
      $responseBody->family_name,
      $responseBody->name,
      $responseBody->phone_number,
      $responseBody->sid,
      $responseBody->sub
    );
  }

  public function authenticateUser(string $code): RedirectResponse{
    $authToken = $this->getAuthToken($code);
    $userData = $this->getUserInfo($authToken);

    $this->userAuthenticator->setDestination($this->configManager->getPostLoginUrl());

    $response = $this->userAuthenticator->authenticateUser(
      $userData->getEmail(),
      $userData->getEmail(),
      $userData->getSid(),
      $authToken
    );

    $this->logger->logAuthenticatedUser($userData->getSid());

    return $response;
  }

  private function getResponseBody(ResponseInterface $response):\stdClass {
    return json_decode($response->getBody()->getContents());
  }

}
