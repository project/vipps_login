<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Service;

use Drupal\Core\TempStore\PrivateTempStoreFactory;

class CustomerSession
{
  private $store;

  public function __construct(PrivateTempStoreFactory $storeFactory)
  {
    $this->store = $storeFactory->get('vipps_login');
  }

  public function setKeyState(string $state): void {
    $this->store->set(StateKey::DATA_KEY_STATE, $state);
  }

  public function getKeyState(): ?string {
    return $this->store->get(StateKey::DATA_KEY_STATE);
  }
}
