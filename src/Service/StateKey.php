<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Service;

class StateKey
{
  /**
   * @var string
   */
  const DATA_KEY_STATE = 'url_state';

  public function generate(): string {
    //TODO add salt for state
    $state = md5(uniqid(strval(rand()), true));

    return substr($state, 0, 20);
  }
}
