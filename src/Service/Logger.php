<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Service;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use \Drupal\Core\StringTranslation\StringTranslationTrait;

class Logger
{
  use StringTranslationTrait;

  private $logger;

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->logger = $loggerChannelFactory->get('vipps_login');
  }

  public function addError(string $message): void {
    $this->logger->error($message);
  }

  public function addMessage(string $message): void {
    $this->logger->log(1, $message);
  }

  public function logAuthenticatedUser(string $userName): void {
    $this->logger->log(1, $this->t("User @username was authenticated", ['username' => $userName]));
  }

}
