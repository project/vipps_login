<?php

declare(strict_types=1);

namespace Drupal\vipps_login\ApiResponseData;

class UserData
{
  private $email;

  private $firstName;

  private $lastName;

  private $name;

  private $phoneNumber;

  private $sid;

  private $sub;

  private $address;

  public function __construct(
    AddressData $address,
    bool $emailVerified,
    string $email,
    string $sid,
    string $sub,
    ?string $firstName,
    ?string $lastName,
    ?string $name,
    ?string $phoneNumber
  )
  {
    if(!$emailVerified) {
      throw new \DomainException('Email is not verified');
    }

    $this->address = $address;
    $this->email = $email;
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->name = $name;
    $this->phoneNumber = $phoneNumber;
    $this->sid = $sid;
    $this->sub = $sub;
  }

  /**
   * @return string
   */
  public function getEmail(): string
  {
    return $this->email;
  }

  /**
   * @return string
   */
  public function getFirstName(): ?string
  {
    return $this->firstName;
  }

  /**
   * @return string
   */
  public function getLastName(): ?string
  {
    return $this->lastName;
  }

  /**
   * @return string
   */
  public function getName(): ?string
  {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getPhoneNumber(): ?string
  {
    return $this->phoneNumber;
  }

  /**
   * @return string
   */
  public function getSid(): string
  {
    return $this->sid;
  }

  /**
   * @return string
   */
  public function getSub(): string
  {
    return $this->sub;
  }

  /**
   * @return AddressData
   */
  public function getAddress(): AddressData
  {
    return $this->address;
  }
}

