<?php

declare(strict_types=1);

namespace Drupal\vipps_login\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\vipps_login\Service\CustomerSession;
use Symfony\Component\HttpFoundation\RequestStack;

class StateSessionChecker implements AccessInterface
{
  protected $request;

  private $customerSession;

  public function __construct(RequestStack $requestStack, CustomerSession $customerSession){
    $this->request = $requestStack->getCurrentRequest();
    $this->customerSession = $customerSession;
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    return AccessResult::allowedIf($this->request->get('state') === $this->customerSession->getKeyState());
  }

}
